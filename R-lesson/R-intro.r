1+100
1+
200  
3 + 5 * 2
(3 + 5) * 2
(3 + ( 5 * ( 2 ^ 2))) # hard to read
3 + 5 * 2 ^ 2 # clear if you remember the rules
3 + 5 * (2 ^ 2) # easier if you forgot some rules
2/100000 # division
5e4 # Note no minus sign
# Mathematical functions
sin(1) # a trigonometic function
log(1) # a natural logarithm
log10(100) # base-10 logarithm
exp(0.5) # e^(1/2)
# Comparing things
0.5 == 1/2 # is equal to?
exp(0.5) == exp(1/2)
e^(1/2)
exp(1)
#use a variable
e = exp(1)
e^(1/2) == exp(0.5)
